/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Version : 50614
 Source Host           : localhost
 Source Database       : xiaom

 Target Server Version : 50614
 File Encoding         : utf-8

 Date: 02/08/2014 21:06:56 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `xm_assess`
-- ----------------------------
DROP TABLE IF EXISTS `xm_assess`;
CREATE TABLE `xm_assess` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `distance` int(10) unsigned NOT NULL DEFAULT '0',
  `van_type` varchar(2) NOT NULL,
  `big_obj` varchar(140) DEFAULT NULL,
  `old_floor` tinyint(3) unsigned NOT NULL,
  `old_lift` tinyint(4) NOT NULL,
  `old_address` varchar(255) DEFAULT NULL,
  `new_floor` tinyint(3) unsigned NOT NULL,
  `new_lift` tinyint(4) NOT NULL,
  `new_address` varchar(255) DEFAULT NULL,
  `assess_price` int(4) unsigned NOT NULL,
  `assess_time` datetime NOT NULL,
  `ipv4` varchar(16) NOT NULL,
  `phone` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `xm_book`
-- ----------------------------
DROP TABLE IF EXISTS `xm_book`;
CREATE TABLE `xm_book` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `old_address` varchar(255) NOT NULL,
  `new_address` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `date_format` int(11) unsigned NOT NULL,
  `van_type` varchar(2) NOT NULL,
  `phone` varchar(14) NOT NULL,
  `assess_price` int(4) DEFAULT NULL,
  `book_time` datetime NOT NULL,
  `ipv4` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
