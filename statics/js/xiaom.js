/**
 * Author: xworm
 * Date: 13-7-30
 * Time: 下午8:45
 */

/* 实例化BlackBox */
box = new BlackBox();

/* 客服QQ数组 */
var qq = [844956521,645655198];

/* 接收判断QQ是否在线的jsonp返回值 */
var online = [];

/* 禁止cookie自动 encoded/decoded */
$.cookie.raw = true;

/* 默认加载 */
$(document).ready(function(){

    switch ($(".nav li.current").index()) {
        case 0 :
            home.init();
            home.commits_scroll(3200,800);
            break;
        case 1 :
            book.init();
            book.check_qq_online(5);
            break;
        case 4 :
            //about.time_line_hover();
            break;
    }

    common.init();
});

/**
 * 首页相关函数对象集
 *
 */
var home = {

    input_ads: $(".move_plan input"),

    init: function(){
        home.buttons_listen();
        home.input_listen();
        home.banner_resize();
        home.enter_listen();
        home.set_focus();

        $(window).resize(function() {
            home.banner_resize();
        });
    },

    /**
     * 首页评论滚动
     *
     * @param time
     * @param speed
     */
    commits_scroll: function (time, speed) {
        var commits = $(".user-quote-list").children("li");

        commits.each(function () {
            $(this).stop().removeAttr("style");
        });
        $(commits[0]).hide().fadeIn(speed/2);

        commits.each(function () {
            var li = $(this);
            setTimeout(function () {
                li.animate({marginTop: "-70px"}, speed);
            }, time * (li.index() + 1));
        });

        setTimeout(function () {
            home.commits_scroll(time, speed);
        }, commits.length * time+speed/2);
    },

    /**
     * 按钮监听
     */
    buttons_listen: function(){
        $("#home-to-step2").click(function(){
            home.begin_assess();
        });
    },

    enter_listen: function(){
        $("#old_ads").keydown(function(event){
            if(event.which == 13) {
                home.begin_assess();
            }
        });

        $("#new_ads").keydown(function(event){
            if(event.which == 13) {
                home.begin_assess();
            }
        })

    },

    begin_assess: function(){
        var flag = true;

        for(var x=0; x<2; x++){
            if($(home.input_ads[x]).val() === "") {
                $(home.input_ads[x]).parent().addClass('error');
                flag = false;
            } else {
                $.cookie(x===0?"xm_old_ads":"xm_new_ads", $(home.input_ads[x]).val(), { expires: 7, path: '/' });
            }
        }

        flag && assess.open(1);
    },

    input_listen: function(){
        home.input_ads.each(function(){
            $(this).focus(function(){
                $(this).parent().removeClass("error");
            });
        });
    },

    set_focus: function(){
        var url_tail = window.location.hash;
        if(url_tail === "#welcome") {
            home.input_ads.first().focus();
        }
    },

    banner_resize: function(){
        var width = ($("body").width());

        if(width < 1200) {
            var left,center,right,gap = (1200-width)/2;
            left = -gap;
            center = 1200*0.22-gap;
            right = width-810+gap;

            $("#welcome").css('background-position',left+'px bottom, '+center+'px 100%, '+right+'px bottom');
        } else {
            $("#welcome").removeAttr('style');
        }
    }

};

/**
 * 关于我们 相关函数对象集
 *
 * @type {{time_line_hover: Function, time_text_scroll: Function}}
 */
var about = {

    /**
     * 时间轴hover效果
     *
    time_line_hover: function(){
        var bubble = $(".big-bubble");
        var dots = $(".bubble");

        dots.each(function(){
            $(this).hover(function(){
                var left = parseInt($(this).css("left"));
                bubble.css("left",left-4);

                about.time_text_scroll(left+1);
            });
        });
    },
    */

    /**
     * 时间轴hover效果下文本框移动控制
     *
     * @param center
     *
    time_text_scroll: function(center){
        var text = $(".time-text");
        var width = text.width();
        var left = center-parseInt(width/2);
        //console.log(left);

        text.stop();
        text.animate({"left":left},300);

    }
    */

};

/**
 * 预约服务 相关函数对象集
 *
 *
 */
var book = {

    qq_links: $(".book-qq"),
    book_name: $("#book-name"),
    book_old_ads: $("#book-old-ads"),
    book_new_ads: $("#book-new-ads"),
    book_date: $("#book-date"),
    book_van: $("#book-van"),
    book_phone: $("#book-phone"),

    /**
     * 初始化 QQ 链接
     */
    init: function(){
        book.qq_links.each(function(){
            $(this).attr("href","http://wpa.qq.com/msgrd?v=3&uin="+qq[$(this).index()]+"&site=qq&menu=yes");
        });

        book.per_deal_form();

        $("#book-btn").click(function(){
            if(book.deal_form() === false)
                return false;
        });

        $("#book-by-web form input").each(function(){
            $(this).focus(function(){
                $(this).parent().removeClass("error");
            });
        });
    },

    /**
     * 检测客服QQ在线状态
     *
     * @param time
     */
    check_qq_online: function(time){
        common.check_qq(qq,function(){
            for(var i=0; i<online.length; i++) {
                online[i] === 0 && book.qq_links.eq(i).removeClass("online").addClass("offline");
                online[i] === 1 && book.qq_links.eq(i).removeClass("offline").addClass("online");
            }
            setTimeout(function(){book.check_qq_online(time);},time*1000);
        });
    },

    deal_form: function(){
        var flag = true, change = false, b_name, b_old_ads, b_new_ads, b_date, b_van, b_phone,b_price;

        b_name = $.trim(book.book_name.val());
        b_old_ads = $.trim(book.book_old_ads.val());
        b_new_ads = $.trim(book.book_new_ads.val());
        b_date = $.trim(book.book_date.val());
        b_phone = /\d+\-\d+|\d+/.exec(book.book_phone.val());
        b_van = book.book_van.find('input[name="van"]:checked').val();
        b_price = $.cookie('xm_as_price');

        if(b_name === "") {
            book.book_name.parent().addClass('error');
            flag = false;
        }else if(b_name !== $.cookie('xm_name')) {
            change = true;
            $.cookie('xm_name', b_name, { expires: 7, path: '/' });
        }

        if(b_old_ads === ""){
            book.book_old_ads.parent().addClass('error');
            flag = false;
        }else if(b_old_ads !== $.cookie('xm_old_ads')) {
            change = true;
            $.cookie('xm_old_ads', b_old_ads, { expires: 7, path: '/' });
        }

        if(b_new_ads === ""){
            book.book_new_ads.parent().addClass('error');
            flag = false;
        }else if(b_new_ads !== $.cookie('xm_new_ads')) {
            change = true;
            $.cookie('xm_new_ads', b_new_ads, { expires: 7, path: '/' });
        }

        if(b_date === ""){
            book.book_date.parent().addClass('error');
            flag = false;
        }else if(b_date !== $.cookie('xm_date')) {
            change = true;
            $.cookie('xm_date', b_date, { expires: 7, path: '/' });
        }

        if(b_phone === null){
            book.book_phone.parent().addClass('error');
            flag = false;
        }else if(b_phone[0] !== $.cookie('xm_phone')) {
            change = true;
            $.cookie('xm_phone', b_phone, { expires: 7, path: '/' });
        }

        if(b_van !== $.cookie('xm_van')) {
            change = true;
            $.cookie('xm_van', b_van, { expires: 7, path: '/' });
        }



        if(b_old_ads === $.cookie('xm_old_ads') && b_new_ads === $.cookie('xm_new_ads') && b_van === $.cookie('xm_van')){
            b_price === undefined && (b_price = 0);
        } else {
            b_price = 0;
        }

        if(flag === true ) {
            if(change === true){
                box.load('book');
                $.post('api.php?method=book&target=web',{name:b_name,old_ads:b_old_ads,new_ads:b_new_ads,date:b_date,van:b_van,phone:b_phone[0],price:b_price},
                    function(data){
                        box.ready('book');
                        if(data !== $.cookie('xm_bk_id')){
                            box.alert("感谢您选择小美，预约已经成功，预约ID号为 <b>"+ data +" </b><br>我们将会在数小时内联系您，请保持手机畅通＃^_^");
                        } else {
                            box.alert("当前预约信息已经提交，请耐心等待小美与您联系。");
                        }
                    },'json');
            } else {
                box.alert("当前预约信息已经提交，请耐心等待小美与您联系。");
            }
        }

        return flag;
    },

    per_deal_form: function(){
        book.book_name.val($.cookie('xm_name'));
        book.book_old_ads.val($.cookie('xm_old_ads'));
        book.book_new_ads.val($.cookie('xm_new_ads'));
        book.book_date.val($.cookie('xm_date'));
        book.book_phone.val($.cookie('xm_phone'));

        if($.cookie('xm_van') === "b")
            book.book_van.find("input[name=van][value=b]").attr("checked",true);
    }
};


var assess = {

    setting:{
        0:["<h2>0.<small>输入新旧居所地址</small></h2>","0%","Start"],
        1:["<h2>1.<small>选择合适的车型</small></h2>","18%","20%"],
        2:["<h2>2.<small>请输入您的楼层</small></h2>","36%","40%"],
        3:["<h2>3.<small>标示您的大件物品</small></h2>","54%","60%"],
        4:["<h2>4.<small>最后一步，确认两地间路程</small></h2>","72%","80%"],
        5:["<h3>小美对本次搬家估价为： </h3>","90%","<a onclick=box.boxClose();>离开</a>"]
    },


    open: function(n){
        if($("#BlackBox").length === 0){
            box.load('load');
            $.get("api.php?method=assess&target=html",function(html){
                box.ready('load');
                box.popup(html,function(){
                    $("#as-distance").focus(function(){$(".as-distance .tooltip").fadeIn();})
                        .blur(function(){$(".as-distance .tooltip").fadeOut();});

                    $(".as-big-checked").delegate(".tag","click",function(){
                        $(this).remove();
                    });

                    $(".as-big a").click(function(){
                        $(".as-big-checked").append('<span class="tag" _xm_oid="'+ $(this).attr("_xm_oid") +'"><span>'+ $(this).html() +'</span><a class="tag-remove-link"></a></span>');
                    });

                    $(".as-minibus").click(function(){
                        $(".as-buggy").removeClass("current");
                        $(this).addClass("current");
                    });

                    $(".as-buggy").click(function(){
                        $(".as-minibus").removeClass("current");
                        $(this).addClass("current");
                    });

                    assess._book_btn();
                    assess.step_in(n);

                });
            });
        }
        else {
            assess.step_in(n);
        }
    },


    step_in: function(n){
        $("#as-step"+n).show();
        $(".as-head").html(assess.setting[n][0]);
        $(".as-foot .car").animate({left:assess.setting[n][1]},500,function(){
            $(".as-foot .num").html(assess.setting[n][2]);
        });

        assess["load_step"+n]();
        assess.next_btn(n+1);
    },

    next_btn: function(n){
        $("#as-to-step"+ (n)).click(function(){
            if(n !== 5){
                if(assess["deal_step"+(n-1)]() !== false)
                    assess.step_out(n-1);

            } else {
                assess["deal_step"+(n-1)]();
                assess.step_out(n-1,function(){
                    assess._load("count");
                    $.get("api.php?method=assess&target=price",function(html){
                        $(".as-price").html(html + ".00");

                        assess._ready("count",function(){
                            assess.step_in(n);
                        },'json');
                    })
                });
            }
        });
    },

    step_out: function(n,callback){
        $("#as-step"+n).addClass("rotateOut").delay(800).hide(0,function(){
            $("#as-step"+n).removeClass("rotateOut");
            callback === undefined ? assess.step_in(n+1) :callback();
        });
    },

    step_end: function(){
        var tel_input = $("#as-phone");

        $("#as-to-end").click(function(){
            tel_input.blur();

            if(tel_input.hasClass('value')) {
                $.get("api.php?method=assess&target=phone",function(result){
                    box.boxClose(function(){
                        box.alert(result.msg);
                    });
                },'json');
            } else {
                box.boxShake();
            }
        });
    },

    load_step1: function(){
        var van = $.cookie('xm_van');
        if(van === 'b'){
            $(".as-minibus").removeClass("current");
            $(".as-buggy").addClass("current");
        }
    },

    deal_step1: function(){
        if($(".as-buggy").hasClass("current") === true){
            $.cookie('xm_van', "b", { expires: 7, path: '/' });
        } else {
            $.cookie('xm_van', "m", { expires: 7, path: '/' });
        }
    },

    load_step2: function(){
        var tmp = $.cookie('xm_floor'), floor=[];
        if(tmp !== undefined) {
            floor = tmp.split(",");
            floor[0] === "true" && $('#old-lift').attr('checked',true);
            floor[1] === "true" && $('#new-lift').attr('checked',true);
            floor[2] !== "0" && $('#old-floor').val(floor[2]);
            floor[3] !== "0" && $('#new-floor').val(floor[3]);
        }
    },

    deal_step2: function(){
        var old_elm = $('#old-floor'),
            old_floor = /\d+/.exec(old_elm.val()),
            old_lift = $('#old-lift').is(':checked');

        var new_elm = $('#new-floor'),
            new_floor = /\d+/.exec(new_elm.val()),
            new_lift = $('#new-lift').is(':checked');

        var out_arr = [];

        if ((old_floor === null && old_lift === false)) {
            old_elm.val("").focus();
            box.boxShake();
            return false;
        }
        else if ((new_floor === null && new_lift === false)) {
            new_elm.val("").focus();
            box.boxShake();
            return false;
        }
        else {
            out_arr.push(old_lift,new_lift);
            old_floor !== null ? out_arr.push(old_floor[0]) : out_arr.push("0");
            new_floor !== null ? out_arr.push(new_floor[0]) : out_arr.push("0");

            $.cookie('xm_floor', out_arr, { expires: 7, path: '/' });
            return true;
        }
    },

    load_step3: function(){
        var big_obj_str = $.cookie('xm_big_obj');
        if(big_obj_str !== "" && big_obj_str !== undefined){
            var big_obj_arr = big_obj_str.split(",");
            var obj_name;

            for(var i=0; i<big_obj_arr.length; i++){
                obj_name = $(".as-big a[_xm_oid="+big_obj_arr[i]+"]").html();
                $(".as-big-checked").append('<span class="tag" _xm_oid="'+ big_obj_arr[i] +'"><span>'+ obj_name +'</span><a class="tag-remove-link"></a></span>');
            }
        }
    },

    deal_step3: function(){
        var big_obj_arr = $(".as-big-checked .tag"), big_num_arr=[];

        big_obj_arr.each(function(){
            big_num_arr.push($(this).attr("_xm_oid"));
        });

        $.cookie('xm_big_obj', big_num_arr, { expires: 7, path: '/' });

    },

    load_step4: function(){
        var distance = $.cookie('xm_distance');
        distance !== undefined && $("#as-distance").val(distance);
    },

    deal_step4: function(){
        var distance = $("#as-distance").val();
        distance = /\d+/.exec(distance);

        distance !== null && $.cookie('xm_distance', distance, { expires: 7, path: '/' });

    },

    load_step5: function(){
        var tel_input = $("#as-phone"),tmp_tel = $.cookie('xm_phone');
        tmp_tel !== undefined && tel_input.val(tmp_tel) && tel_input.addClass('value');

        tel_input.blur(function(){
            tmp_tel = /(\d{11}|^((|\d{3,4}-)\d{7,8}(|-\d{1,4}))$)/.exec(tel_input.val().replace(/－/,'-'));
            if (tmp_tel === null) {
                tel_input.removeClass('value');
            } else {
                tel_input.addClass('value');
                $.cookie('xm_phone', tmp_tel[0], { expires: 7, path: '/' })
            }
        });

        assess.step_end();
    },


    _load: function(flg){
        var html = $("#assess").parent().html();
        $("body").append("<div id='assess-tmp' style='display: none'>"+html+"</div>");

        box.boxClose();
        box.load(flg);
    },

    _ready: function(flg,callback){
        var tmp = $("#assess-tmp");

        box.ready(flg);
        box.popup(tmp.html(),function(){
            callback();
        });
        tmp.remove();
    },

    _book_btn: function(){
        var book_btn = $("#as-to-book");

        if(online[0] === 1){
            book_btn.attr("href",'http://wpa.qq.com/msgrd?v=3&uin='+qq[0]+'&site=qq&menu=yes');
            book_btn.attr("target","_blank");
            return;
        }
        if(online[1] === 1){
            book_btn.attr("href",'http://wpa.qq.com/msgrd?v=3&uin='+qq[1]+'&site=qq&menu=yes');
            book_btn.attr("target","_blank");
        } else {
            book_btn.attr("href",'book.html#book-by-tel');
        }

    }
};

var common = {
    init: function(){
        common.foot_qq_set();
        common.float_up2_set();
        common.float_help_set();
    },

    check_qq: function(qq,callback){
        var url="http://webpresence.qq.com/getonline?Type=1&" + qq.join(":") + ":";

        $.ajax({
            type: "get",
            async: false,
            url: url,
            mimeType: "text/html",
            dataType: "jsonp",
            error: callback
        });
    },

    foot_qq_set: function(){
        var qq_btn = $(".QQ");

        common.check_qq(qq,function(){
            if(online[0] === 1){
                qq_btn.attr("href",'http://wpa.qq.com/msgrd?v=3&uin='+qq[0]+'&site=qq&menu=yes');
                return;
            }
            if(online[1] === 1){
                qq_btn.attr("href",'http://wpa.qq.com/msgrd?v=3&uin='+qq[1]+'&site=qq&menu=yes');
            }
        });
    },

    float_up2_set: function(){
        var screen_height = $(window).height(), scroll_height, up2_btn = $("#J_gotoTop");
        up2_btn.hide();

        $(window).scroll(function(){
            scroll_height = $(window).scrollTop();
            scroll_height > screen_height*0.7 ? up2_btn.fadeIn(100) : up2_btn.fadeOut(100);
        });

        up2_btn.click(function(){
            $("body").animate({scrollTop:0},500);
        })

    },

    float_help_set: function(){
        var help_btn = $("#J_onlineHelp");

        help_btn.hover(function(){
            help_btn.find(".J_help").css("background-position", "0 50%")
        },function(){
            help_btn.find(".J_help").css("background-position", "0 0")
        })
    }
    
};


//加载51统计
//document.write ('<div style="display: none"><script language="javascript" type="text/javascript" src="http://js.users.51.la/16749846.js"></script></div>');

//百度统计
//var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
//document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3Fc102ab127e59db6009a443e1f9b46ef5' type='text/javascript'%3E%3C/script%3E"));
