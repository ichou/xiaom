<?php
/**
 * Author: xworm
 * Date: 13-8-21
 * Time: 下午10:37
 */

class Book_model extends Model {

    public function record($arr)
    {

        $arr['book_time'] = date("Y-m-d H:i:s");
        $arr['ipv4'] = get_ip();

        $this->db->set_table("xm_book");
        return $this->db->insert($arr);

    }

    public function read($id = null)
    {
        if($id === null) return false;

        $this->db->set_table("xm_book");
        $where = "`id`=$id";
        return $this->db->select('*', $where);
    }

}