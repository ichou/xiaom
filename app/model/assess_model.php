<?php
/**
 * Author: xworm
 * Date: 13-8-16
 * Time: 上午3:23
 */

class Assess_model extends Model {

    public function record($arr)
    {

        $arr['assess_time'] = date("Y-m-d H:i:s");
        $arr['ipv4'] = get_ip();

        $this->db->set_table("xm_assess");
        return $this->db->insert($arr);

    }

    public function read($id = null)
    {
        if($id === null) return false;

        $this->db->set_table("xm_assess");
        $where = "`id`=$id";
        return $this->db->select('*', $where);
    }

    public function update($id = null, $arr = array())
    {
        if($id === null) return false;

        $arr['assess_time'] = date("Y-m-d H:i:s");

        $this->db->set_table("xm_assess");
        $this->db->update($arr,'id='.$id);
        return true;
    }

}