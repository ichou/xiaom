<?php defined('INDEX_PAGE') or die('no entrance');
/**
 * Author: xworm
 * Date: 13-8-7
 * Time: 上午12:00
 */

class Assess extends Controller {

    public function __construct()
    {
        $this->Assess = new Assess_model();

        $this->price_arr = get_conf('price');

    }

    public function html()
    {
        //sleep(0.5);
        render('assess');
    }

    public function price()
    {
        $book = array();
        $xm_tmp = explode(',',get_cookie('xm_floor','false,false,0,0'),4);

        $book['distance'] = (int)get_cookie('xm_distance',10);
        $book['big_obj'] = get_cookie('xm_big_obj');
        $book['van_type'] = get_cookie('xm_van','m');
        $book['old_lift'] = $xm_tmp[0] === "true" ? 1 : 0;
        $book['new_lift'] = $xm_tmp[1] === "true" ? 1 : 0;
        $book['old_floor'] = $xm_tmp[2];
        $book['new_floor'] = $xm_tmp[3];
        $book['old_address'] = get_cookie('xm_old_ads');
        $book['new_address'] = get_cookie('xm_new_ads');

        //sleep(2);
        $book['assess_price'] = $this->_calculate($book);

        $old_id = get_cookie('xm_as_id');
        if($old_id !== null) {
            if($this->_check_repeat($old_id,$book)){
                out_put($book['assess_price']);
                return;
            };
        }

        $assess_id = $this->Assess->record($book);
        out_put($book['assess_price']);

        set_cookie('xm_as_id',$assess_id,7,'/');
        set_cookie('xm_as_price',$book['assess_price'],7,'/');

    }

    public function phone()
    {
        $book['id'] = get_cookie('xm_as_id');
        $book['phone'] = get_cookie('xm_phone');

        $book['id'] || out_put(array('stat'=>400,'msg'=>'(╯°Д°）╯︵ ┻━┻   出错了，节操不见了'));
        $book['phone'] || out_put(array('stat'=>400,'msg'=>'(╯°Д°）╯︵ ┻━┻    出错了，节操不见了'));

        $ret = $this->Assess->update($book['id'],array('phone'=>$book['phone']));
        $ret && out_put(array('stat'=>200,'msg'=>'电话号码已提交，感谢您的支持＃^_^'));
    }

    private function _calculate($book)
    {
        $big_obj_arr = explode(',', $book['big_obj']);

        $price = $this->price_arr['van'][$book['van_type']];
        $book['distance'] > 10 && $price += ($book['distance']-10)*$this->price_arr['distance'];

        if($big_obj_arr[0] !== "")
            foreach($big_obj_arr as $v)
                $price += $this->price_arr['big_obj'][$v];

        if($book['old_lift'] === 0 && $book['old_floor']>3)
            $price += ($book['old_floor']-3)*$this->price_arr['floor'];

        if($book['new_lift'] === 0 && $book['new_floor']>3)
            $price += ($book['new_floor']-3)*$this->price_arr['floor'];

        return $price;
    }

    private function _check_repeat($id,$book)
    {
        $check_arr = $this->Assess->read($id);
        $diff_arr = array_diff($book,$check_arr[0]);

        return empty($diff_arr);
    }
}