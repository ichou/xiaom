<?php defined('INDEX_PAGE') or die('no entrance');
/**
 * Author: xworm
 * Date: 13-8-21
 * Time: 下午10:35
 */

class Book extends Controller {

    public function __construct()
    {
        $this->Book = new Book_model();
    }

    public function web()
    {
        $book = array();
        $book['name'] = isset($_POST['name']) ? $_POST['name'] : $_COOKIE['xm_name'];
        $book['old_address'] = isset($_POST['old_ads']) ? $_POST['old_ads'] : $_COOKIE['xm_old_ads'];
        $book['new_address'] = isset($_POST['new_ads']) ? $_POST['new_ads'] : $_COOKIE['xm_new_ads'];
        $book['date'] = isset($_POST['date']) ? $_POST['date'] : $_COOKIE['xm_date'];
        $book['date_format'] = strtotime($book['date']);
        $book['van_type'] = isset($_POST['van']) ? $_POST['van'] : $_COOKIE['xm_van'];
        $book['phone'] = isset($_POST['phone']) ? $_POST['phone'] : $_COOKIE['xm_phone'];
        $book['assess_price'] = isset($_POST['price']) ? $_POST['price'] : 0;

        $check_id = $_COOKIE['xm_bk_id'];
        if(isset($check_id))
        {
            if($this->_check_repeat($check_id,$book))
            {
                out_put($check_id);
                return;
            }
        }

        $book_id = $this->Book->record($book);

        set_cookie('xm_bk_id',$book_id,7,'/');
        out_put($book_id);
    }

    private function _check_repeat($id,$book)
    {
        $check_arr = $this->Book->read($id);
        $diff_arr = array_diff($book,$check_arr[0]);

        return empty($diff_arr);
    }
}