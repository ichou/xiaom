<?php defined('INDEX_PAGE') or die('no entrance');
/**
 * Author: xworm
 * Date: 13-9-8
 * Time: 下午6:58
 */

class Html extends Controller {

    public function __construct()
    {
        $this->url = get_conf('url');
    }

    public function home()
    {
        render('header',array(
            'page'  => 'home',
            'title' => '小美搬家｜高新区尚美家政服务部',
        ));
        render('home');
        render('footer');
    }

    public function book()
    {
        render('header',array(
            'page'  => 'book',
            'title' => '预约服务',
        ));
        render('book');
        render('footer');
    }

    public function vans()
    {
        render('header',array(
            'page'  => 'vans',
            'title' => '预约服务',
        ));
        render('vans');
        render('footer');
    }

    public function price()
    {
        render('header',array(
            'page'  => 'price',
            'title' => '资费标准',
        ));
        render('price');
        render('footer');
    }

    public function about()
    {
        render('header',array(
            'page'  => 'about',
            'title' => '关于我们',
        ));
        render('about');
        render('footer');
    }
}