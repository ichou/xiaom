<?php
/**
 * Author: xworm
 * Date: 13-8-8
 * Time: 下午11:52
 */

$app_config['url'] = array(
    'web'   => 'http://www.xiaomei.com',
    'static'=> 'http://s.xiaomei.com',
    'mobile'=> 'http://m.xiaomei.com'

);

$app_config['price'] = array(
    'distance'  => '3',
    'floor'     => '10',
    'van'       => array('m' => 90, 'b' => 160),
    'big_obj'   => array(
        0 => 100,
        1 => 90,
        2 => 120,
        3 => 60,
        4 => 50,
        5 => 50,
        6 => 80,
        7 => 60,
        8 => 80,
        9 => 50,
        10 => 50
    )

);

